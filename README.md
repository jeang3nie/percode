Small commandline tool for percent encoding/decoding url strings (or other
data).

# Building
```sh
cargo build --release
```
# Usage
```sh
percode - 0.1.0
Percent encode url's

Usage:
    percode [OPTIONS] [URL]...

    Options:
        -d, --decode        Decode instead of encode
        -h, --help          Display this help menu
        -v, --version       Display program version
```
