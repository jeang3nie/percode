//! Small commandline tool for percent encoding/decoding url strings (or other
//! data).
//!
//! # Building
//! ```sh
//! cargo build --release
//! ```
//! # Usage
//! ```sh
//! percode - 0.1.0
//! Percent encode url's
//!
//!Usage:
//!    percode [OPTIONS] [URL]...
//!
//!    Options:
//!        -d, --decode        Decode instead of encode
//!        -h, --help          Display this help menu
//!        -v, --version       Display program version
//! ```

use getopts::Options;
use std::env;
use std::io::prelude::*;

fn usage(progname: &str, opts: &Options) {
    println!("{} - {}\nPercent encode url's\n", progname, env!("CARGO_PKG_VERSION"));
    let brief = format!("Usage:\n    {} [OPTIONS] [URL]...", progname);
    print!("{}", opts.usage(&brief));
}

fn main() {
    let args: Vec<String> = env::args().collect();
    let progname = env!("CARGO_PKG_NAME");

    let mut opts = Options::new();
    opts.optflag("d", "decode", "Decode instead of encode");
    opts.optflag("h", "help", "Display this help menu");
    opts.optflag("v", "version", "Display program version");

    let matches = match opts.parse(&args[1..]) {
        Ok(m) => m,
        Err(e) => {
            eprintln!("{}", e);
            return;
        }
    };
    if matches.opt_present("h") {
        usage(&progname, &opts);
        return;
    }
    if matches.opt_present("v") {
        println!("{}", env!("CARGO_PKG_VERSION"));
        return;
    }

    if matches.free.is_empty() {
        for url in std::io::stdin().lock().lines() {
            let url = url.expect("Cannot read line");
            if matches.opt_present("d") {
                match urlencoding::decode(&url) {
                    Ok(s) => println!("{}", s),
                    Err(e) => eprintln!("{}", e),
                }
            } else {
                println!("{}", urlencoding::encode(&url));
            }
        }
    } else {
        for url in &matches.free {
            if matches.opt_present("d") {
                match urlencoding::decode(&url) {
                    Ok(s) => println!("{}", s),
                    Err(e) => eprintln!("{}", e),
                }
            } else {
                println!("{}", urlencoding::encode(&url));
            }
        }
    }
}
